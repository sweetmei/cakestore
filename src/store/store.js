import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        total: 0,
        allData: [],
        allPrice: 0,
        shopCar: []
    },
    mutations: {
        saveData: (state, data) => {
            data.categoryList.forEach((val, i) => {
                val.spuList.map((v, ind) => {
                    v.count = 0;
                    v.price = 0;
                    return v;
                })
            });
            state.allData = data;
        },
        changeDatas: (state, data) => {
            var { smallind, bidInd } = {...data }
            state.allData.categoryList[bidInd].spuList.map((v, i) => {
                if (v.spuId == smallind) {
                    v.count = 1;
                    v.price = v.count * v.originPrice;
                    state.total++;
                    state.allPrice += v.originPrice;

                }
            });
        },
        changecount(state, data) {
            let { type, smallind, bidInd } = {...data }
            if (bidInd !== undefined) {
                state.allData.categoryList[bidInd].spuList.map((v, i) => {
                    if (v.spuId == smallind) {
                        if (type === "add") {
                            v.count += 1;
                            v.price = (v.count * v.originPrice).toFixed(2);
                            state.allPrice += v.originPrice;
                            state.total++;
                            if (state.shopCar.indexOf(v) == -1) {
                                state.shopCar.push(v);
                            }
                        } else {
                            v.count--;
                            if (v.count < 1) {
                                state.shopCar.splice(i, 1)
                            }
                            v.price = v.count * v.originPrice;
                            state.allPrice -= v.originPrice;
                            state.total--
                        }
                    }

                });
            } else {
                state.shopCar.forEach((v, i) => {
                    if (v.spuId == smallind) {
                        if (type === "add") {
                            v.count += 1;
                            v.price = (v.count * v.originPrice).toFixed(2);
                            state.allPrice += v.originPrice;
                            state.total++;
                            if (state.shopCar.indexOf(v) == -1) {
                                state.shopCar.push(v);
                            }
                        } else {
                            v.count--;
                            if (v.count < 1) {
                                state.shopCar.splice(i, 1)
                            }
                            v.price = v.count * v.originPrice;
                            state.allPrice -= v.originPrice;
                            state.total--
                        }
                    }
                })
            }

        }
    },
    actions: {
        saveData(context, data) {
            context.commit("saveData", data)
        },
        changeCount(context, data1) {
            context.commit("changeDatas", data1)
        },
        changeCounts(context, data1) {
            context.commit("changecount", data1)
        }
    }
})